// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>

#define LONG 50
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}
CMundoServidor::CMundoServidor()
{
	Init();
	flag=0;
	
}

CMundoServidor::~CMundoServidor()
{
	//Enviar mensaje de Fin al Logger para que se cierre
	char cadenaFin[16];
	sprintf(cadenaFin,"Fin del juego\n"); //Para asegurar que lo que haya en cadenaFin sea un string
	write(fd,cadenaFin,strlen(cadenaFin)+1);
	close(fd);

	//Cerrar tuberia de las coordenadas
	write(fd_coordenadas,cadenaFin,strlen(cadenaFin)+1);
	close(fd_coordenadas);	

	//Cerrar tuberia de teclas
	close(fd_teclas);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	if(flag==1){
		
		esfera2.centro.x=esfera.centro.x+1;
		esfera2.centro.y=esfera.centro.y+1;
		//esfera2.velocidad.x=-esfera.velocidad.x;
		//esfera2.velocidad.y=-esfera.velocidad.y;	
		esfera2.velocidad.y=1;
		esfera2.velocidad.x=2;	
		flag++;
		
	}
	if(flag>=1)esfera2.Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	if(flag>=1)
		esfera2.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(esfera2);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=3+3*rand()/(float)RAND_MAX;
		esfera.velocidad.y=3+3*rand()/(float)RAND_MAX;
		puntos2++;

		char cadena2[LONG];
		sprintf(cadena2,"Jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);	
		write(fd,cadena2,strlen(cadena2)+1);
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-3-3*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-3-3*rand()/(float)RAND_MAX;
		puntos1++;
		
		char cadena1[LONG];
		sprintf(cadena1,"Jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);	
		write(fd,cadena1,strlen(cadena1)+1);
	}

	
	//Construir la cadena de texto con los datos a enviar	
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	
	//Enviamos los datos por la tubería
	int write_error=write(fd_coordenadas,cad,strlen(cad)+1);
	if(write_error==-1){
		perror("Error al escribir en la tubería de las coordenadas\n");
		exit(1);
	}	

	//Fin del juego si alguno de los jugadores llega a 3 puntos
	//if(puntos1==3 || puntos2==3)exit(1);
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void CMundoServidor::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//ABRIR TUBERÍA LOGGER
	fd=open("/tmp/tuberia",O_WRONLY);
	if(fd==-1){
		perror("Error al abrir la tubería del Logger\n");
		exit(1);
	}
	
	//ABRIR TUBERÍA COORDENADAS
	fd_coordenadas=open("/tmp/tuberiaCoordenadas", O_WRONLY);
	if(fd_coordenadas==-1){
		perror("Error al abrir la tubería de las coordenadas\n");
		exit(1);
	}
	
	//ABRIR TUBERÍA TECLAS
	fd_teclas=open("/tmp/tuberiaTeclas", O_RDONLY);
	if(fd_teclas==-1){
		perror("Error al abrir la tubería de las teclas\n");
		exit(1);
	}	
	
	pthread_create(&thid1, NULL, hilo_comandos, this);
}




void CMundoServidor::RecibeComandosJugador()
{
	while (1) {
		usleep(10);
        	char cad[100];
        	read(fd_teclas, cad, sizeof(cad));
        	unsigned char key;
            
		sscanf(cad,"%c",&key);
		if(cad[0]=='F')exit(1);
		else
        		if(key=='s')jugador1.velocidad.y=-4;
        		if(key=='w')jugador1.velocidad.y=4;
        		if(key=='l')jugador2.velocidad.y=-4;
        		if(key=='o')jugador2.velocidad.y=4;
	}
}

